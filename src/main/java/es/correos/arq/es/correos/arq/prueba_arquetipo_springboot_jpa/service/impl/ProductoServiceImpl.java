package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Productos;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.ProductosMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.ProductosReducidoMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.repository.ProductosRepository;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.ProductosService;

@Service
public class ProductoServiceImpl implements ProductosService{

	@Autowired
	private ProductosRepository productosRepository;
	
	private ProductosMapper productosMapper;
	
	private ProductosReducidoMapper productosReducidoMapper;
	
	@Override
	public List<ProductosDTO> listarProductos() {
		return productosMapper.listProductosConvProductosDTO(productosRepository.findAll());
	}

	@Override
	public Productos obtenerProductosPorID(Integer id) throws Exception {
		return productosRepository.findById(id).orElseThrow(()->new Exception("Producto no encontrado"));
	}

	@Override
	public ProductosDTO registrarProductos(ProductosReducidoDTO request) throws Exception {
		return productosMapper.productosConvProductosDTO(productosRepository
				.save(productosReducidoMapper
						.productosReducidoDTOConvProductos(request)));
	}

	@Override
	public ProductosDTO modificarProductos(ProductosReducidoDTO request, Integer id) throws Exception {
		Productos productos = productosReducidoMapper.productosReducidoDTOConvProductos(request);
		return productosMapper.productosConvProductosDTO(productosRepository.findById(id)
		        .map(productoUpdate -> {
		        	productoUpdate.setCategoriaId(productos.getCategoriaId());
		        	productoUpdate.setDescripcion(productos.getDescripcion());
		        	productoUpdate.setExistencia(productos.getExistencia());
		        	productoUpdate.setPrecioUnit(productos.getPrecioUnit());
		        	productoUpdate.setProveedorId(productos.getProveedorId());
		        	productosRepository.save(productoUpdate);
		            return productoUpdate;
		    }).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Producto no encontrado")));
	}

	@Override
	public void eliminarProductos(Integer id){
		productosRepository.deleteById(id);
	}

}

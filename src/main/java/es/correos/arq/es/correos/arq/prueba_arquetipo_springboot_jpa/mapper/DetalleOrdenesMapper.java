package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.DetalleOrdenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenesDTO;

@Mapper
public interface DetalleOrdenesMapper {

	DetalleOrdenesDTO detalleOrdenesConvDetalleOrdenesDTO (DetalleOrdenes detalleOrdenes);
	List<DetalleOrdenesDTO> listDetalleOrdenesConvDetalleOrdenesDTO(List<DetalleOrdenes> list);
	DetalleOrdenes detalleOrdenesDTOConvDetalleOrdenes (DetalleOrdenesDTO detalleOrdenesDTO);
	List<DetalleOrdenes> listDetalleOrdenesDTOConvDetalleOrdenes (List<DetalleOrdenesDTO> list);
	
}

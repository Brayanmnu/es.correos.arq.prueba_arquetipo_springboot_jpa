package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenesDTO;

public interface DetalleOrdenesService {
	//public Iterable<DetalleOrdenesDTO> listarDetalleOrdenesPorOrden(Long idOrden);
	
	public DetalleOrdenesDTO registrarDetalleOrdenes(DetalleOrdenReducidoDTO request) throws Exception;
	//public DetalleOrdenesDTO modificarDetalleOrdenes(DetalleOrdenReducidoDTO request, Integer idOrden, Integer idDetalle) throws Exception;
	public void elminarDetalleOrdenes(DetalleOrdenesDTO request);
}

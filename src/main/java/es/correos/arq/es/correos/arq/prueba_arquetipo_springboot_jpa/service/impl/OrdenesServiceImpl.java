package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Ordenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.OrdenesMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.OrdenesReducidoMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.repository.OrdenesRepository;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.OrdenesService;

@Service
public class OrdenesServiceImpl implements OrdenesService{
	@Autowired
	private OrdenesRepository ordenesRepository;

	private OrdenesMapper ordenesMapper;
	
	private OrdenesReducidoMapper ordenesReducidoMapper;
	
	@Override
	public List<OrdenesDTO> listarOrdenes() {
		return ordenesMapper.listOrdenesConvOrdenesDTO(ordenesRepository.findAll());
	}

	@Override
	public OrdenesDTO registrarOrdenes(OrdenesReducidoDTO request) throws Exception{
		return ordenesMapper.ordenesConvOrdenesDTO(ordenesRepository
				.save(ordenesReducidoMapper
						.ordenesDTOConvOrdenes(request)));
	}

	@Override
	public OrdenesDTO modificarOrdenes(OrdenesReducidoDTO request, Integer id) throws Exception{
		Ordenes ordenes = ordenesReducidoMapper.ordenesDTOConvOrdenes(request);
		
		return ordenesMapper.ordenesConvOrdenesDTO (ordenesRepository.findById(id)
				.map(ordenesUpdate->{
					ordenesUpdate.setClienteId(ordenes.getClienteId());
					ordenesUpdate.setDescuento(ordenes.getDescuento());
					ordenesUpdate.setEmpleadoId(ordenes.getEmpleadoId());
					ordenesUpdate.setFechaOrden(ordenes.getFechaOrden());
					ordenesRepository.save(ordenes);
					return ordenesUpdate;
				}) .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Orden no encontrada")));
	}

	@Override
	public Ordenes obtenerOrdenPorID(Integer id) throws Exception {
		return ordenesRepository.findById(id).orElseThrow(()->new Exception("Orden no encontrada"));
	}

	@Override
	public void eliminarOrdenes(Integer id) {
		ordenesRepository.deleteById(id);
	}


}

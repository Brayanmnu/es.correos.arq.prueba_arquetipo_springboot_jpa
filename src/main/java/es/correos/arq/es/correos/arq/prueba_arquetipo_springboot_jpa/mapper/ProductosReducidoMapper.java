package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Productos;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosReducidoDTO;

@Mapper
public interface ProductosReducidoMapper {
	ProductosReducidoDTO productosConvProductosReducidoDTO (Productos productos);
	List<ProductosReducidoDTO> listProductosConvProductosReducidoDTO (List<Productos> lista);
	Productos productosReducidoDTOConvProductos (ProductosReducidoDTO productosDTO);
	List<Productos> listProductosReducidoDTOConvProductos(List<ProductosReducidoDTO> lista);
}

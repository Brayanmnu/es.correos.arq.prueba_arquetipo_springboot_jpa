package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto;

import lombok.Data;

@Data
public class DetalleOrdenesDTO {
	private OrdenesDTO orden;
	private Integer detalleId;
	private ProductosDTO producto;
	private Integer cantidad;
}

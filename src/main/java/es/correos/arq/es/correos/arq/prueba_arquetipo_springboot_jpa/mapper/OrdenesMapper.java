package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Ordenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesDTO;

@Mapper
public interface OrdenesMapper {
	OrdenesDTO ordenesConvOrdenesDTO(Ordenes ordenes);
	List<OrdenesDTO> listOrdenesConvOrdenesDTO(List<Ordenes> lista);
	Ordenes ordenesDTOConvOrdenes(OrdenesDTO ordenesDto);
	List<Ordenes> listOrdenesDTOConvOrdenes(List<OrdenesDTO> lista);
}

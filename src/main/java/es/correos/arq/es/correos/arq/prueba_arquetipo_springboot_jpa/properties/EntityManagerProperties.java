/**
 * 
 */
package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.properties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author jbarriov
 *
 */
@NoArgsConstructor
@Getter
@Setter
public class EntityManagerProperties {
	private String[] packagesToScan;
}	

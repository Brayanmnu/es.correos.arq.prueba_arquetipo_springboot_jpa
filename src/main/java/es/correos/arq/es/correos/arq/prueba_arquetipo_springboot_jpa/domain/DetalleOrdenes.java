package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="DETALLE_ORDENES")
public class DetalleOrdenes {
	@EmbeddedId
	private DetalleOrdenesId detallesId;
	@ManyToOne
	@JoinColumn(name="PRODUCTOID",nullable = false)
	private Productos producto;
	@Column(name="CANTIDAD",nullable = false)
	private Integer cantidad;
}

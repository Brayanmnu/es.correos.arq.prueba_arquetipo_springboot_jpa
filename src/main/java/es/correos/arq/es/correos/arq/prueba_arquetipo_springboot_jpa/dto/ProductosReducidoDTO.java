package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductosReducidoDTO {
	private Integer proveedorId;
	private Integer categoriaId;
	private Character descripcion;
	private BigDecimal precioUnit;
	private Integer existencia;
}

package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.exceptions.handler;

import es.correos.arch.boot.core.exception.ErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ApplicationErrorCode implements ErrorCode {
	ENTITY_EXISTS_CONFLICT("Entity exists - conflict");

	private final String reasonPhrase;
}

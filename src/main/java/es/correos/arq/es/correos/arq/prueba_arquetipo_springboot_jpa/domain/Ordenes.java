package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain;


import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="ORDENES")
public class Ordenes {
	@Id
	@Column(name="ORDENID",nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer ordenId;
	@Column(name="EMPLEADOID",nullable = false)
	private Integer empleadoId;
	@Column(name="CLIENTEID",nullable = false)
	private Integer clienteId;
	@Column(name="FECHAORDEN",nullable = false)
	private Date fechaOrden;
	@Column(name="DESCUENTO",nullable = true)
	private Integer descuento;
}

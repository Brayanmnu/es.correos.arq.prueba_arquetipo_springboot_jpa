package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto;

import lombok.Data;

@Data
public class DetalleOrdenReducidoDTO {
	private Integer ordenId;
	private Integer productoId;
	private Integer cantidad;
}

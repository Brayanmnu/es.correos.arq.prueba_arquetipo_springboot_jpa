package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Ordenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesReducidoDTO;

@Mapper
public interface OrdenesReducidoMapper {
	OrdenesReducidoDTO ordenesConvOrdenesDTO(Ordenes ordenes);
	List<OrdenesReducidoDTO> listOrdenesConvOrdenesDTO(List<Ordenes> lista);
	Ordenes ordenesDTOConvOrdenes(OrdenesReducidoDTO ordenesDto);
	List<Ordenes> listOrdenesDTOConvOrdenes(List<OrdenesReducidoDTO> lista);
}

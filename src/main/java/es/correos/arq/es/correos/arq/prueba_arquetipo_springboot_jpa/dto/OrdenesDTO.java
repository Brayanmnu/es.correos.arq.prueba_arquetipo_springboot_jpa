package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto;


import java.io.Serializable;
import java.sql.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdenesDTO implements Serializable{
	private Integer ordenId;
	private Integer empleadoId;
	private Integer clienteId;
	private Date fechaOrden;
	private Integer descuento;
}

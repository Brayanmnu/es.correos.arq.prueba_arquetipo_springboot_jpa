package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenesDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.OrdenesMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.OrdenesReducidoMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.ProductosMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.ProductosReducidoMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.DetalleOrdenesService;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.OrdenesService;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.ProductosService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
@Api(value = "/api", tags = "Operaciones con usuarios")
@Log4j2
public class ApiController {

	@Autowired
	OrdenesService ordenesService;
	
	@Autowired
	ProductosService productosService;

	@Autowired
	DetalleOrdenesService detalleOrdenesService;
	
	
	@ApiOperation(value = "Acceso a  todos los usuario.",
			notes = "Se pide el listado de todas los ordenes dados de alta en la BBDD.")
	@GetMapping(value="/ordenesListar", headers = "Accept=application/json")
	@ApiResponse(code = 401, message = "No se encontro el resultado")

	public ResponseEntity<List<OrdenesDTO>> listarOrdenes(){
		return  new ResponseEntity<>(ordenesService.listarOrdenes(),HttpStatus.OK);
	}
	
	@PostMapping("/ordenRegistrar")
	public ResponseEntity<OrdenesDTO> registrarOrden(@RequestBody OrdenesReducidoDTO request) throws Exception {
		return  new ResponseEntity<>(ordenesService
				.registrarOrdenes(request), HttpStatus.CREATED);
	}
	
	@PutMapping("ordenModficar/{id}")
	public ResponseEntity<OrdenesDTO> modificarOrden(@PathVariable("id") Integer id
			,@RequestBody OrdenesReducidoDTO request) throws Exception{
		return new ResponseEntity<>(ordenesService
				.modificarOrdenes(request,id), HttpStatus.OK);
	}
	
	@DeleteMapping("ordenEliminar/{id}")
	public void eliminarOrden(@PathVariable("id") Integer id) throws Exception{ 
		ordenesService.eliminarOrdenes(id);
	}
	
	@GetMapping("/productosListar")
	public List<ProductosDTO> listarProductos(){
		return productosService.listarProductos();
	}
	
	@PostMapping("/productoRegistrar")
	public  ResponseEntity<ProductosDTO> registrarProductos (@RequestBody ProductosReducidoDTO request) 
			throws Exception {
		return new ResponseEntity<>(productosService
				.registrarProductos(request), HttpStatus.CREATED);
	}
	
	@PutMapping("productoModificar/{id}")
	public ResponseEntity<ProductosDTO> modificarProductos(@PathVariable("id") Integer id
			, @RequestBody ProductosReducidoDTO request) throws Exception{ 
		return new ResponseEntity<>(productosService
				.modificarProductos(request,id), HttpStatus.OK);
	}
	
	@DeleteMapping("productoEliminar/{id}")
	public void eliminarProducto(@PathVariable("id") Integer id)throws Exception{ 
		productosService.eliminarProductos(id);
	}
	
	@PostMapping("/detalleOrdenRegistrar")
	public ResponseEntity<DetalleOrdenesDTO> registrarDetalleOrden(@RequestBody 
			DetalleOrdenReducidoDTO request) throws Exception{
		return new ResponseEntity<>(detalleOrdenesService.registrarDetalleOrdenes(request), HttpStatus.OK);
	}
	
	/*@PutMapping("detalleOrdenModificar/{idOrden}/{idDetalle}")
	public ResponseEntity<DetalleOrdenesDTO> modificarDetalleOrden(@PathVariable("idOrden") Integer idOrden
			, @RequestBody DetalleOrdenReducidoDTO request,@PathVariable("idDetalle") Integer idDetalle) throws Exception{
		Ordenes orden = ordenesService.obtenerOrdenPorID(request.getOrdenId());
		Productos producto = productosService.obtenerProductosPorID(request.getProductoId());
		DetalleOrdenes detalleOrdenes = new DetalleOrdenes();
		detalleOrdenes.setOrden(orden);
		detalleOrdenes.setProducto(producto);
		detalleOrdenes.setCantidad(request.getCantidad());
		DetalleOrdenesDTO detalleOrdenesDTO = detalleOrdenesMapper
				.detalleOrdenesConvDetalleOrdenesDTO(detalleOrdenesService.modificarDetalleOrdenes(request));
		return new ResponseEntity<>(detalleOrdenesDTO, HttpStatus.OK);
	}*/
	
}
	
	
	
	
	
	
	
	
package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.DetalleOrdenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.DetalleOrdenesId;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenReducidoDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.DetalleOrdenesDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper.DetalleOrdenesMapper;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.repository.DetalleOrdenesRepository;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.DetalleOrdenesService;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.OrdenesService;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service.ProductosService;

@Service
public class DetalleOrdenesServiceImpl implements DetalleOrdenesService{

	@Autowired
	private DetalleOrdenesRepository detalleOrdenesRepository;
	
	@Autowired
	private OrdenesService ordenesService;
	@Autowired
	private ProductosService productosService;
	
	private DetalleOrdenesMapper detalleOrdenesMapper;
	
	@Override
	public DetalleOrdenesDTO registrarDetalleOrdenes(DetalleOrdenReducidoDTO request) throws Exception {
		DetalleOrdenesId detalleOrdenesId = new DetalleOrdenesId();
		detalleOrdenesId.setOrden(ordenesService.obtenerOrdenPorID(request.getOrdenId()));

		DetalleOrdenes detalleOrdenes = new DetalleOrdenes();
		detalleOrdenes.setCantidad(request.getCantidad());
		detalleOrdenes.setDetallesId(detalleOrdenesId);
		detalleOrdenes.setProducto(productosService.obtenerProductosPorID(request.getProductoId()));
		return detalleOrdenesMapper.detalleOrdenesConvDetalleOrdenesDTO(detalleOrdenesRepository
				.save(detalleOrdenes));
	}

	/*@Override
	public DetalleOrdenesDTO modificarDetalleOrdenes(DetalleOrdenReducidoDTO request, Integer idOrden, Integer idDetalle) throws Exception {
		
		return detalleOrdenesMapper.detalleOrdenesConvDetalleOrdenesDTO(detalleOrdenesRepository.findById(detalleOrdenesId)
				.map(detalleOrdenesUpdate->{
					detalleOrdenesUpdate.setCantidad(detalleOrden.getCantidad());
					detalleOrdenesUpdate.setProducto(detalleOrden.getProducto());
					return detalleOrdenesUpdate;
				}).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND,"Orden no encontrada")));
	}
*/
	@Override
	public void elminarDetalleOrdenes(DetalleOrdenesDTO request) {
		// TODO Auto-generated method stub
	}

}

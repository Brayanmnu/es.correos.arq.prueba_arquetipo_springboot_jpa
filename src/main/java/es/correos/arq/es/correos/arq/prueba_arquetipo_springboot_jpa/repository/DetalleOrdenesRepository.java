package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.DetalleOrdenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.DetalleOrdenesId;

public interface DetalleOrdenesRepository extends JpaRepository<DetalleOrdenes, DetalleOrdenesId>{
	
	public Optional<DetalleOrdenes> findById(DetalleOrdenesId detallesId); 
}

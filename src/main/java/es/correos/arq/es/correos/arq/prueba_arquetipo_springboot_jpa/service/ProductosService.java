package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service;

import java.util.List;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Productos;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosReducidoDTO;
public interface ProductosService {
	public List<ProductosDTO> listarProductos();
	public Productos obtenerProductosPorID(Integer id) throws Exception;
	public ProductosDTO registrarProductos(ProductosReducidoDTO request) throws Exception;
	public ProductosDTO modificarProductos(ProductosReducidoDTO request, Integer id) throws Exception; 
	public void eliminarProductos(Integer id) ;
}

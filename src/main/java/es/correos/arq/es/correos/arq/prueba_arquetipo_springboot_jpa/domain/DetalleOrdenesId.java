package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class DetalleOrdenesId implements Serializable{
	@ManyToOne
	@JoinColumn(name="ORDENID",nullable = false)
	private Ordenes orden;
	@Column(name="DETALLEID",nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer detalleId;
}

package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.service;

import java.util.List;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Ordenes;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesDTO;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.OrdenesReducidoDTO;

public interface OrdenesService {
	public List<OrdenesDTO> listarOrdenes();
	public Ordenes obtenerOrdenPorID(Integer id) throws Exception;
	public OrdenesDTO registrarOrdenes(OrdenesReducidoDTO request) throws Exception; 
	public OrdenesDTO modificarOrdenes(OrdenesReducidoDTO request, Integer id) throws Exception;
	public void eliminarOrdenes(Integer id);
}

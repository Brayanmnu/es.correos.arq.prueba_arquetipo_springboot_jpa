package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Productos;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto.ProductosDTO;

@Mapper
public interface ProductosMapper {
	ProductosDTO productosConvProductosDTO (Productos productos);
	List<ProductosDTO> listProductosConvProductosDTO (List<Productos> lista);
	Productos productosDTOConvProductos (ProductosDTO productosDTO);
	List<Productos> listProductosDTOConvProductos(List<ProductosDTO> lista);
}

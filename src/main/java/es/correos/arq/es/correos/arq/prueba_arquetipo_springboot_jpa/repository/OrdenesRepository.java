package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Ordenes;

public interface OrdenesRepository extends JpaRepository<Ordenes,Integer>{

	public Optional<Ordenes> findById(Integer ordenId);
}

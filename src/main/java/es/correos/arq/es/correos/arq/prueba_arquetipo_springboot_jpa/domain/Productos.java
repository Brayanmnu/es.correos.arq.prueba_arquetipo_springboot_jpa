package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="PRODUCTOS")
public class Productos {
	@Id
	@Column(name="PRODUCTOID",nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer productoId;
	@Column(name="PROVEEDORID",nullable = false)
	private Integer proveedorId;
	@Column(name="CATEGORIAID",nullable = false)
	private Integer categoriaId;
	@Column(name="DESCRIPCION",nullable = true)
	private Character descripcion;
	@Column(name="PRECIOUNIT",nullable = false)
	private BigDecimal precioUnit;
	@Column(name="EXISTENCIA",nullable = false)
	private Integer existencia;
}

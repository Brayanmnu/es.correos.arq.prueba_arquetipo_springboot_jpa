package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.domain.Productos;

public interface ProductosRepository extends JpaRepository<Productos, Integer>{
	public Optional<Productos> findById(Integer productoId);

}

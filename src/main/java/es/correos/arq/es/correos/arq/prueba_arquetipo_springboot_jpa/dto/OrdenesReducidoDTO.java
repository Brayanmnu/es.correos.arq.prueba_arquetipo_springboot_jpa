package es.correos.arq.es.correos.arq.prueba_arquetipo_springboot_jpa.dto;

import lombok.Data;

@Data
public class OrdenesReducidoDTO {
	private Integer empleadoId;
	private Integer clienteId;
	private Integer descuento;

}
